import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {QuizzProvider} from "../../providers/quizz/quizz";
import {Question} from "../../models/question";

@Component({
    selector: 'page-quizz',
    templateUrl: 'quizz.html',
})
export class QuizzPage {

    public number: number;
    public difficulty: string;
    public questions: Array<Question> = [];
    public nbQuestions: number;
    public currentQuestion: number = 0;
    public answer: string = "";
    public answered: boolean = false;
    public questionMessage: string;
    public chrono: number = 5;
    public interval;
    public disabled: boolean = false;
    public score: number = 0;
    public finish: boolean = false;

    ionViewDidLoad() {
        this.number = this.navParams.get("number");
        this.difficulty = this.navParams.get("difficulty");
        this.getQuestions();
    }

    public async getQuestions() {
        let result = await this.QuizzProvider.getQuizz(this.number, this.difficulty);
        this.questions = result['results'];
        this.nbQuestions = this.questions.length;

        for (let question of this.questions) {
            // Push the right answer in a random place with the wrong answers.
            let n = question['incorrect_answers'].length;
            let nPlace = Math.floor(Math.random() * n) + 1;
            question['incorrect_answers'].splice(nPlace, 0, question['correct_answer']);
        }
        this.startChrono();
    }

    public async startChrono() {
        this.interval = setInterval(() => {
            if (this.chrono > 0) {
                this.chrono--;
            } else {
                this.questionMessage = "Le temps est écoulé";
                clearInterval(this.interval);
                this.answered = true;
                this.disabled = true;
            }
        }, 1000);
    }

    public async doAnswer() {
        if (this.answer !== "" || this.chrono === 0) {
            this.answered = true;
            this.disabled = true;
            clearInterval(this.interval);

            if (this.answer === this.questions[this.currentQuestion].correct_answer) {
                this.questionMessage = "Bonne réponse !";
                this.score = this.score + (1 * this.chrono);
                console.log(this.score)
            } else {
                this.questionMessage = "Mauvaise réponse !"
            }
        }
    }

    public async goToNextQuestion() {
        if (this.currentQuestion < this.nbQuestions - 1) {
            this.answered = false;
            this.questionMessage = "";
            this.disabled = false;
            this.currentQuestion++;
        }

        this.chrono = 5;
        this.startChrono();
    }

    public async showResults() {
        this.finish = true;
    }

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public QuizzProvider: QuizzProvider
    ) {
    }
}
