import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {QuizzPage} from "../quizz/quizz";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    public error: string;
    public number: number = 5;
    public difficulty: string;

    public launchQuizz(number, difficulty) {
        if (!this.number && !this.difficulty) {
            this.error = "Vous devez faire des choix dans la vie.";
            return;
        }

        if (!this.number) {
            this.error = "Vous devez choisir le nombre de questions.";
            return;
        }

        if (!this.difficulty) {
            this.error = "Vous devez choisir une difficulté.";
            return;
        }

        this.navCtrl.push(QuizzPage, {
            number: number,
            difficulty: difficulty
        });
    }

    constructor(public navCtrl: NavController) {
    }

}
