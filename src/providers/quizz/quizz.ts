import {Http} from "@angular/http";
import {Injectable} from '@angular/core';

@Injectable()
export class QuizzProvider {

    constructor(
        public http: Http,
    ) {
    }

    public async getQuizz(number, difficulty): Promise<Object> {
        let quizz = await this.http.get(
            "https://opentdb.com/api.php?amount=" + number + "&difficulty=" + difficulty
        ).toPromise();

        if (quizz.json()) {
            return quizz.json();
        } else  {
            return;
        }
    }
}
